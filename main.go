package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Printf("Numero de CPUs: %v\n al inicio", runtime.NumCPU())
	fmt.Printf("Numero de Goroutine: %v\n al inicio", runtime.NumGoroutine())

	var wg sync.WaitGroup

	wg.Add(2)

	go func() {
		fmt.Println("Hola desde de la primera Goroutina")

		wg.Done()
	}()

	go func() {
		fmt.Println("Hola desde la segunda Goroutina")

		wg.Done()
	}()

	fmt.Printf("Numero de CPUs: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de Goroutine: %v\n", runtime.NumGoroutine())

	wg.Wait()
	fmt.Println("A punto de finalizar.")

	fmt.Printf("Numero de CPUs: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de Goroutine: %v\n", runtime.NumGoroutine())
}
